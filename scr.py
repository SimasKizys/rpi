#!/usr/bin/python
#
# Example of using buttons on an "ITEAD RPi LCD1602 v2.0" board
#
# Based on Adafruit_Python_CharLCD/examples/char_lcd_plate.py example
#   v1.0    20/10/15
#
#   David Meiklejohn
#   Gooligum Electronics

import time
import RPi.GPIO as GPIO
import Adafruit_CharLCD as LCD
import os


# Define pushbutton switch GPIOs
buttonIO = [8, 7, 10, 9, 11]    # SW1..5


# configure I/O
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(buttonIO, GPIO.IN)






print 'Press Ctrl-C to quit.'
while True:
	# Loop through each button and check if it is pressed.
	for b in range(5):
		if GPIO.input(buttonIO[b])==0 :
			if(b==4):
				os.system('sudo python /home/pi/Documents/LCD/eik.py')
				found = False
				while (found == False):
					if GPIO.input(buttonIO[b])==0 :
						if(b==0):
							print("yo")
							os.system('sudo python /home/pi/Documents/LCD/eik.py')
							found = True
						if(b==1):
							os.system('sudo /home/pi/Documents/LCD/butt2.sh')
							found = True
						if(b==2):
							os.system('sudo /home/pi/Documents/LCD/butt3.sh')
							found = True
						if(b==3):
							os.system('sudo /home/pi/Documents/LCD/butt4.sh')
							found = True				