#!/usr/bin/python
import time
import RPi.GPIO as GPIO
import Adafruit_CharLCD as LCD
import os

BTN1_CMD = 'sudo python /home/pi/Documents/LCD/eik.py'
BTN2_CMD = 'sudo python /home/pi/Documents/LCD/showIp.py'
BTN3_CMD = 'sudo python /home/pi/Documents/LCD/mypy.py "SOME TEST TEXT"'
BTN4_CMD = 'sudo python /home/pi/Documents/LCD/bootLCD.py'
# Define pushbutton switch GPIOs
buttonIO = [8, 7, 10, 9, 11]    # SW1..5


# configure I/O
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(buttonIO, GPIO.IN)

print 'Press Ctrl-C to quit.'
while True:
	# Loop through each button and check if it is pressed.
	time.sleep(0.05)
	for b in range(5):
		if GPIO.input(buttonIO[b])==0 :
			if(b==4):
				found = False
				while (found == False):
					time.sleep(0.05)
					for d in range(5):
						if GPIO.input(buttonIO[d])==0 :
							if(d==0):
								
								os.system(BTN1_CMD)
								found = True
							if(d==1):
								
								os.system(BTN2_CMD)
								found = True
							if(d==2):
								
								os.system(BTN3_CMD)
								found = True
							if(d==3):
								
								os.system(BTN4_CMD)
								found = True